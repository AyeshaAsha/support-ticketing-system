from flask import request, jsonify, session
from flask_restful import Api, Resource
from flask_cors import cross_origin
from werkzeug.security import generate_password_hash, check_password_hash
from app import app
from app.model import db, User, Ticket_info, Reply_info
from functools import wraps
import jwt, datetime, re

#RE for email
regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
#Decorator for token
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'token' in request.headers:
            token = request.headers['token']
        if not token:
            return jsonify({'message' : 'Token is missing!!'}), 401
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"]) # decoding the payload to fetch the stored details
            current_user = User.query.filter_by(username = data['user']).first()
        except:
            return jsonify({'message': 'Token is invalid!!'}), 401

        return  f(current_user, *args, **kwargs)
    return decorator

#Home
@app.route('/')
def index():
    return jsonify({'message':'Support Ticketing System'}), 200
#Registration
@app.route('/register', methods=["POST"])
@cross_origin()
def register():
    if request.is_json:
        req = request.get_json()
        name = req.get("name")
        email = req.get("email")
        if re.fullmatch(regex, email):
            pass
        else:
            return jsonify({'message': 'Invalid Email Format'}), 400
        address = req.get("address")
        username = req.get("username")
        password = req.get("password")
        c_password = req.get("confirm_password")
        if password != c_password:
            return jsonify({'message': "Password doesn't match"}), 400
        result =  User.query.filter_by(email=email).first()
        if result:
            return jsonify({'message': 'Email is not available'}), 400
        result =  User.query.filter_by(username=username).first()
        if result:
            return jsonify({'message': 'Username is not available'}), 400

        password = generate_password_hash(password)
        data = User(name=name, email=email, address=address, username=username, password=password)
        db.session.add(data)
        db.session.commit()
        output = []
        output.append({
            'username' : data.username,
            'email' : data.email,
        })
        return jsonify({'user': output}), 200
    else:
        return jsonify({'message': 'Request body must be JSON'}), 400
#login
@app.route('/login', methods=["POST"])
@cross_origin()
def login():
    auth = request.authorization
    if not auth:
        return jsonify({'message': 'Authorization is required'}), 400
    if not auth.username:
        return jsonify({'message': 'Username is required'}), 400
    if not auth.password:
        return jsonify({'message': 'Password is required'}), 400
    user = User.query.filter_by(username=auth.username).first()
    if user is None or check_password_hash(user.password, auth.password)!=True:
        return jsonify({'message': 'Please enter valid username and password'}), 400
    payload = {'user': auth.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes = 30)}
    token = jwt.encode(payload, app.config['SECRET_KEY'], "HS256")
    return jsonify({'token': token, 'name':user.name, 'role':user.role}), 200
#Fetch user info
@app.route('/user_info', methods=["POST"])
@cross_origin()
@token_required
def view_user_info(current_user):
    if current_user.role != "Super Admin" and current_user.role != "Support Admin":
        return jsonify({'message': 'Access denied'}), 400
    users = User.query.all()
    output = []
    for user in users:
        output.append({
            'id': user.id,
            'name': user.name,
            'email': user.email,
            'address': user.address,
            'role': user.role
        })
    return jsonify({'users': output}), 200
#Edit user role
@app.route('/edit_user_role/<user_id>', methods=["POST"])
@cross_origin()
@token_required
def edit_user_role(current_user, user_id):
    if current_user.role != "Super Admin":
        return jsonify({'message': 'Access denied'}), 400
    if request.is_json:
        req = request.get_json()
        role = req.get("role")
        user = User.query.filter_by(id=user_id).first()
        if not user:
            return jsonify({'message': 'No registered user'}), 400
        user.role = role
        db.session.commit()
        output = []
        output.append({
            'name' : user.name,
            'email' : user.email,
            'role' : user.role
        })
        return jsonify({'user': output}), 200
    else:
        return jsonify({'message': 'Request body must be JSON'}), 400
#Open ticket
@app.route('/add_ticket', methods=["POST"])
@cross_origin()
@token_required
def add_ticket(current_user):
    if current_user.role == "Support Person":
        return jsonify({'message': 'Access denied'}), 400
    if request.is_json:
        req = request.get_json()
        name = req.get("name")
        email = req.get("email")
        sub = req.get("subject")
        ticket_type = req.get("ticket_type")
        message = req.get("message")
        if current_user.role == "Super Admin" or current_user.role == "Support Admin":
            u_id = req.get("user_id")
            if not u_id:
                return  jsonify({'message': 'Need User Id'}), 400
        else:
            u_id = current_user.id

        data = Ticket_info(name=name, email=email, ticket_type=ticket_type, sub=sub, message=message, user_id=u_id)
        db.session.add(data)
        db.session.commit()
        output = []
        output.append({
            'ticket_type': data.ticket_type,
            'name': data.name,
            'email': data.email,
            'subject': data.sub,
            'message': data.message,
            'user_id': data.user_id
            })
        return jsonify({'ticket': output}), 200
    else:
        return jsonify({'message': 'Request body must be JSON'}), 400
#Fetch ticket info
@app.route('/ticket_info', methods=["POST"])
@cross_origin()
@token_required
def view_ticket_info(current_user):
    if current_user.role == "Super Admin" or current_user.role == "Support Admin" or current_user.role == "Support Person":
        info = Ticket_info.query.all()
    else:
        info = Ticket_info.query.filter_by(user_id=current_user.id)
    if not info:
        return jsonify({'message': 'No opened ticket'}), 400
    else:
        output = []
        for i in info:
            output.append({
                'id': i.id,
                'name': i.name,
                'email': i.email,
                'subject': i.sub,
                'ticket_type' : i.ticket_type,
                'message' : i.message
            })
        return jsonify({'tickets': output})
#Message reply
@app.route("/reply/<ticket_id>", methods=["POST"])
@cross_origin()
@token_required
def reply_ticket(current_user, ticket_id):
    if request.is_json:
        req = request.get_json()
        reply_msg = req.get("reply_msg")
        info = Ticket_info.query.filter_by(id=ticket_id).first()
        if not info:
            return jsonify({'message': 'No opened ticket against this id'}), 400
        if current_user.role == "Super Admin" or current_user.role == "Support Admin" or current_user.role == "Support Person":
            res = Reply_info.query.filter_by(ticket_id=ticket_id).order_by(Reply_info.id.desc()).first()
            if res != None and res.recipient == None:
                res.recipient = current_user.role
            db.session.commit()
            data = Reply_info(sender=current_user.role, message=reply_msg, recipient=info.name, ticket_id=ticket_id)
        else:
            data = Reply_info(sender=current_user.name, message=reply_msg, ticket_id=ticket_id)
        db.session.add(data)
        db.session.commit()
        output=[]
        output.append({
            'sender': data.sender,
            'reply': data.message
        })
        return jsonify({'reply': output}), 200
    else:
        return jsonify({'message': 'Request body must be JSON'}), 400
#Fetch replied messages
@app.route("/reply_info/<ticket_id>", methods=["POST"])
@cross_origin()
@token_required
def all_reply(current_user, ticket_id):
    output = []
    info = Reply_info.query.filter_by(ticket_id=ticket_id)
    if not info:
        return jsonify({'message': 'No reply yet!'}), 401
    for i in info:
        output.append({
            'sender': i.sender,
            'message' : i.message,
            'recipient': i.recipient
        })
    return jsonify({'messages': output}), 200
#logout
@app.route("/logout", methods=["POST"])
@cross_origin()
@token_required
def logout(current_user):
    session.clear()
    return jsonify({'message': 'Logged out'}), 200
